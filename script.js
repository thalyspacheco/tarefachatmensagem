var send = document.querySelector(".btn-send")
var hist = document.querySelector(".historic")

send.addEventListener("click", () => {
    var msg_txt = document.querySelector(".msg-input").value
    if (msg_txt != '') {
        //criando elementos
        var msg_chat = document.createElement('div')
        var p = document.createElement('p')
        var opt = document.createElement('div')
        var btn1 = document.createElement('button')
        var btn2 = document.createElement('button')
        //atribuindo classes
        msg_chat.classList.add("msg-chat")
        p.classList.add("chat")
        opt.classList.add("options")
        btn1.classList.add("edit")
        btn2.classList.add("exclude")
        btn1.textContent = "Editar"
        btn2.textContent = "Excluir"
        p.textContent = msg_txt
        //add na pag
        hist.append(msg_chat)
        msg_chat.append(p)
        msg_chat.append(opt)
        opt.append(btn1)
        opt.append(btn2)
    }

    btn2.addEventListener("click", (e) => {
        e.currentTarget.parentElement.parentElement.remove()
    })
})



